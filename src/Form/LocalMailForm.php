<?php

namespace Drupal\local_mail\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\local_mail\Utils\Utility;

/**
 * Implementing system configuration forms.
 */
class LocalMailForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return Utility::MODULE_NAME . '_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    // Form constructor.
    $form = parent::buildForm($form, $form_state);
    // Default settings.
    $config = $this->config(Utility::MODULE_NAME . '.settings');

    // Source text field.
    $form['number_of_days_to_keep_mail_log_files'] = [
      '#type' => 'number',
      '#min' => 0,
      '#title' => $this->t('Maximum number of days to keep mail log:'),
      '#default_value' => $config->get(Utility::MODULE_NAME . '.number_of_days_to_keep_mail_log_files'),
      '#description' => $this->t('If set to "0", all mail logs will remain. (* Pay attention to the disk capacity!)'),
    ];

    // Is Token Replacement.
    $form['is_token_replacement'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Enable token replacement.'),
      '#default_value' => $config->get(Utility::MODULE_NAME . '.is_token_replacement'),
      '#description' => $this->t('If checked, token replacement will be enabled for the entire text of the email. (* Experimentally function.)'),
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $config = $this->config(Utility::MODULE_NAME . '.settings');
    $config->set(Utility::MODULE_NAME . '.number_of_days_to_keep_mail_log_files', $form_state->getValue('number_of_days_to_keep_mail_log_files'));
    $config->set(Utility::MODULE_NAME . '.is_token_replacement', $form_state->getValue('is_token_replacement'));
    $config->save();
    return parent::submitForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      Utility::MODULE_NAME . '.settings',
    ];
  }

}
