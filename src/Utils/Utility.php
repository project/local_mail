<?php

namespace Drupal\local_mail\Utils;

/**
 * User Media Controller.
 */
class Utility {

  const MODULE_NAME = 'local_mail';

  /**
   * Create directory.
   */
  public static function createDirectory(string $directory) {
    if (!\file_exists($directory) || !\is_dir($directory) || !\is_writable($directory)) {
      $message = '';
      if (!\mkdir($directory, 0777)) {
        $message = \implode(' ', [
          'Message from "@module_name" module:',
          'Not have the permission to create the directory "@directory".',
          'Grant write permission to this parent directory or create it yourself.',
        ]);
      }
      if (!\is_writable($directory)) {
        $message = \implode(' ', [
          'Message from "@module_name" module:',
          'Grant write permission to the directory "@directory".',
        ]);
      }
      if ($message !== '') {
        \Drupal::logger(self::MODULE_NAME)->debug($message, [
          '@module_name' => self::MODULE_NAME,
          '@directory' => $directory,
        ]);
        return FALSE;
      }
    }
    return TRUE;
  }

  /**
   * Remove directories.
   */
  public static function removeDirectories(string $directory) {
    $files = \array_diff(\scandir($directory), ['.', '..']);
    foreach ($files as $file) {
      $path = $directory . '/' . $file;
      if (\is_dir($path)) {
        self::removeDirectories($path);
      }
      else {
        \unlink($path);
      }
    }
    return \rmdir($directory);
  }

  /**
   * Purge logs.
   */
  public static function purgeLogs(string $directory, int $number_of_days_to_keep_mail_log_files) {
    $date = \date('Y-m-d', \strtotime('-' . $number_of_days_to_keep_mail_log_files . 'days'));
    $child_dirs = \array_diff(\scandir($directory), ['.', '..']);
    foreach ($child_dirs as $dir) {
      $path = $directory . '/' . $dir;
      if (!\is_dir($path)) {
        continue;
      }
      if ($dir <= $date) {
        self::removeDirectories($path);
      }
    }
  }

}
