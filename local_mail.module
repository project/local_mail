<?php

/**
 * @file
 * Local Mail Module.
 */

use Drupal\Component\Render\MarkupInterface;
use Drupal\Core\Datetime\DrupalDateTime;
use Drupal\local_mail\Utils\Utility;

/**
 * Implements hook_mail_alter().
 */
function local_mail_mail_alter(&$message) {
  // Get configurations.
  $config = \Drupal::config(Utility::MODULE_NAME . '.settings');
  $number_of_days_to_keep_mail_log_files = (int) $config->get(Utility::MODULE_NAME . '.number_of_days_to_keep_mail_log_files');
  $is_token_replacement = $config->get(Utility::MODULE_NAME . '.is_token_replacement') ?? FALSE;

  // Token service.
  $token = \Drupal::token();

  // Get site mail address.
  $site_mail = \Drupal::config('system.site')->get('mail');

  // Get current time.
  $drupal_date_time = new DrupalDateTime();
  $date = $drupal_date_time->format('Y-m-d');
  $hour = $drupal_date_time->format('H');
  $now = $drupal_date_time->format('Y-m-d H:i:s.u');
  $now_for_file_name = $drupal_date_time->format('Y-m-d_H-i-s.u');

  // Logs directory.
  $logs_directory = __DIR__ . '/logs';

  // Creation of save directory.
  $save_directory = $logs_directory . '/' . $date;
  if (!Utility::createDirectory($save_directory)) {
    return;
  }

  // Make of directory.
  $hour_plus_1 = ((int) $hour) + 1;
  if ($hour_plus_1 > 23) {
    $hour_plus_1 = 0;
  }
  $hour = \sprintf('%02d', $hour);
  $hour_plus_1 = \sprintf('%02d', $hour_plus_1);
  $save_directory .= '/hour_' . $hour . '-' . $hour_plus_1;
  if (!Utility::createDirectory($save_directory)) {
    return;
  }

  // Get mail data.
  $headers = $message['headers'] ?? [];
  $body = $message['body'] ?? [];

  // Array of log data.
  $data = [];

  // Set data of mail info text.
  $data[] = 'Send date and time: ' . $now;

  // Insert a blank line.
  $data[] = '';

  // Extract header informations.
  $data[] = '---- BEGIN HEADER ----';
  foreach ($headers as $key => $value) {
    if ($is_token_replacement) {
      $value = $token->replace($value);
    }
    $data[] = $key . ': ' . $value;
  }
  $data[] = '---- END HEADER ------';

  // Insert a blank line.
  $data[] = '';

  // Extract destination informations.
  $data[] = '---- BEGIN DESTINATION ----';
  foreach ([
    'from',
    'to',
    'cc',
    'bcc',
    'reply-to',
  ] as $key) {
    if (!isset($message[$key])) {
      continue;
    }
    $value = '';
    if ($value === '' && !empty($message[$key])) {
      $value = $message[$key];
      if ($is_token_replacement) {
        $value = $token->replace($value);
      }
    }
    if ($value === '' && $key === 'to') {
      $value = $site_mail;
    }
    if ($value === '') {
      $value = '[is empty value]';
    }
    $data[] = \ucfirst($key) . ': ' . $value;
  }
  $data[] = '---- END DESTINATION ------';

  // Insert a blank line.
  $data[] = '';

  // Extract others informations.
  $data[] = '---- BEGIN OTHER ----';
  foreach ([
    'langcode',
    'subject',
  ] as $key) {
    if (!isset($message[$key])) {
      continue;
    }
    $value = '';
    if ($value === '' && !empty($message[$key])) {
      $value = $message[$key];
      if ($is_token_replacement) {
        $value = $token->replace($value);
      }
    }
    if ($value === '') {
      $value = '[is empty value]';
    }
    $data[] = \ucfirst($key) . ': ' . $value;
  }
  $data[] = '---- END OTHER ------';

  // Insert a blank line.
  $data[] = '';

  // Extract body.
  $data[] = '---- BEGIN BODY ----';
  foreach ($body as $value) {
    $string = '';
    if ($value instanceof MarkupInterface) {
      $string = $value->__toString();
    }
    elseif (\is_string($value)) {
      $string = $value;
    }
    if ($string !== '') {
      if ($is_token_replacement) {
        $string = $token->replace($string);
      }
      $data[] = \preg_replace('/<!--[\s\S]*?-->/s', '', $string);
    }
  }
  $data[] = '---- END BODY ------';

  // Save to text file.
  $to = '';
  if ($to === '' && !empty($message['to'])) {
    $to = $message['to'];
    $to = $token->replace($to);
  }
  if ($to === '') {
    $to = $site_mail;
  }
  if ($to === '') {
    $to = '[unknown]';
  }
  $save_file_name = $save_directory . '/' . $now_for_file_name . '-' . $to . '.eml';
  \file_put_contents($save_file_name, \implode("\n", $data));

  // Purge logs.
  if ($number_of_days_to_keep_mail_log_files > 0) {
    Utility::purgeLogs($logs_directory, $number_of_days_to_keep_mail_log_files);
  }
}
